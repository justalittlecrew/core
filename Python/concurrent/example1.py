import concurrent.futures
 
def fib(n):
    if n < 2:
        return 1
    return fib(n - 1) + fib(n - 2)
 
 
with concurrent.futures.ProcessPoolExecutor() as executor:
    s1 = executor.submit(fib, 10)  # Return future object
    s2 = executor.submit(fib, 20)
    s3 = executor.submit(fib, 5)
    s4 = executor.submit(fib, 8)
 
print(s1.result())
print(s2.result())
print(s3.result())
print(s4.result())
