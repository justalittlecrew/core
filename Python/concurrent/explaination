Example ProcessPoolExecutor and ThreadPoolExecutor
Let's say that we're writing a web application that processes image uploads and resizes them. We will use aiohttp.web, a web framework based on asyncio, and Pillow for the image processing.

A first version, which has some issues that will be outlined below, would look like this:

import io

from PIL import Image
from aiohttp import web


def resize(image_file):
    """Perform the actual resizing using Pillow blocking functions"""
    img = Image.open(image_file)
    img.thumbnail((100, 100), Image.ANTIALIAS)
    buf = io.BytesIO()
    img.save(buf, 'PNG')
    return buf.getvalue()


class Handler(web.View):

    async def get(self):
        return web.Response(body=b'''
         <!DOCTYPE html>
         <html>
         <h1>Resize an image</h1>
         <form action="/" method="post" enctype="multipart/form-data">
           <label for="image">Image</label>
           <input id="image" name="image" type="file" />
           <input type="submit" />
         </form>
         </html>''', content_type='text/html')

    async def post(self):
        data = await self.request.post()
        thumbnail = resize(data['image'].file)
        return web.Response(body=thumbnail, content_type='image/png')


app = web.Application()
app.router.add_route('*', '/', Handler)
web.run_app(app)
The problem here is that the resize function will lock the entire web application process until it has finished resizing the image. No concurrent connections will be allowed during the resize operation. This is expected, since it's a blocking operation being executed in the context of a single-threaded, asynchronous application.

To overcome the issue, we can offload the image processing function to an executor, which can be created with the following code:

from concurrent.futures import ProcessPoolExecutor

class Handler(web.View):
    def __init__(self, request):
        super().__init__(request)
        self.executor = ProcessPoolExecutor()
We will then submit our task to the executor:

    async def post(self):
        data = await self.request.post()
        thumbnail = await self.request.app.loop.run_in_executor(self.executor, resize, data['image'].file.read())
        return web.Response(body=thumbnail, content_type='image/png')
