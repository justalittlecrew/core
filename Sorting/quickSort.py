# The quick sort uses divide and conquer to gain the same advantages as the merge sort, while not using additional storage

# We begin by incrementing leftmark until we locate a value that is greater than the pivot value. 
# We then decrement rightmark until we find a value that is less than the pivot value. 
# At this point we have discovered two items that are out of place with respect to the eventual split point.


# At the point where rightmark becomes less than leftmark, we stop. The position of rightmark is now the split point. 
# The pivot value can be exchanged with the contents of the split point and the pivot value is now in place.
# In addition, all the items to the left of the split point are less than the pivot value, and all the items to the right of the split point are greater than the pivot value. 
# The list can now be divided at the split point and the quick sort can be invoked recursively on the two halves.

def quickSort(alist):
   quickSortHelper(alist,0,len(alist)-1)

def quickSortHelper(alist,first,last):
   if first<last:

       splitpoint = partition(alist,first,last)

       quickSortHelper(alist,first,splitpoint-1)
       quickSortHelper(alist,splitpoint+1,last)


def partition(alist,first,last):
   pivotvalue = alist[first]

   leftmark = first+1
   rightmark = last

   done = False
   while not done:

       while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
           leftmark = leftmark + 1

       while alist[rightmark] >= pivotvalue and rightmark >= leftmark:
           rightmark = rightmark -1

       if rightmark < leftmark:
           done = True
       else:
           temp = alist[leftmark]
           alist[leftmark] = alist[rightmark]
           alist[rightmark] = temp

   temp = alist[first]
   alist[first] = alist[rightmark]
   alist[rightmark] = temp


   return rightmark

alist = [54,26,93,17,77,31,44,55,20]
quickSort(alist)
print(alist)
